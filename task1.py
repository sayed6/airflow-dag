from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

import pandas as pd
import numpy as np
import csv

def first_task():    
    driver=webdriver.Chrome(ChromeDriverManager().install())
    driver.maximize_window()
    driver.get("https://www.moneycontrol.com/markets/global-indices/")
    driver.implicitly_wait(10)

    row = driver.find_elements(By.XPATH,"//*[@class='mctable1 n18_res_table responsive tbl_scroll_resp']//tbody//tr")
    row_length = print(len(row))
    column = driver.find_elements(By.XPATH,"//*[@class='mctable1 n18_res_table responsive tbl_scroll_resp']//thead//tr//th")
    column_length = print(len(column))
    head = driver.find_elements(By.XPATH,"//*[@class='mctable1 n18_res_table responsive tbl_scroll_resp']//thead//tr//th")
    rows = driver.find_elements(By.XPATH,"//*[@id='mc_content']/div/div[4]/div/div/div[1]/div/table/tbody/tr")
        
    Headers = []
    column_1 = []
    column_2 = []
    column_3 = []
    column_4 = []
    column_5 = []
    column_6 = []

    for header in head:
        Headers.append(header.text)
    del Headers[2:4]
    del Headers[-1]
    print(Headers)

    for row in rows:
        u = row.find_element(By.XPATH,"./td[1]").text
        if 'MARKETS' in u:
            continue
        column_1.append(u)
    print(column_1)
    for row in rows:
        v = row.find_element(By.XPATH,"./td[2]").text
        if v == ' ':
            continue
        x = v.replace(',', "")
        column_2.append(float(x))
    print(column_2)
    for row in rows:
        w = row.find_element(By.XPATH,"./td[5]").text
        if w == ' ':
            continue
        column_3.append(w)
    print(column_3)
    for row in rows:
        x = row.find_element(By.XPATH,"./td[6]").text
        if x == ' ':
            continue
        column_4.append(x)
    print(column_4)
    for row in rows:
        y = row.find_element(By.XPATH,"./td[7]").text
        if y == ' ':
            continue
        column_5.append(y)
    print(column_5)
    for row in rows:
        z = row.find_element(By.XPATH,"./td[8]").text
        if z == ' ':
            continue
        b = z.replace(',', "")
        column_6.append(float(b))
    print(column_6)

    array1 = np.array(column_2)
    array2 = np.array(column_6)
    subtracted_array = np.subtract(array1, array2)
    subtracted = list(subtracted_array)
    print(subtracted)


    dict = {'Name':column_1, 'Current Value':column_2, 'Open':column_3,'High':column_4, 'Low':column_5, 'Prev.Close':column_6, 'Delta':subtracted}
    df = pd.DataFrame(dict) 
    df.to_csv('test.csv')

    driver.quit()
first_task()
